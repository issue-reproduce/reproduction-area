## Vowels

á é í ó ú

```markdown
- [relative link](2016-05-11_15-40-00.jpg)
- [explicit relative link](./2016-05-11_15-40-00.jpg)
- [absolute link](/2016-05-11_15-40-00.jpg)
```

- [relative link](2016-05-11_15-40-00.jpg)
- [explicit relative link](./2016-05-11_15-40-00.jpg)
- [absolute link](/2016-05-11_15-40-00.jpg)

```markdown
![relative link](foo/paint-tanuki.png)

---

![explicit relative link](./foo/paint-tanuki.png)

---

![absolute link](/foo/paint-tanuki.png)
```

![relative link](foo/paint-tanuki.png)

---

![explicit relative link](./foo/paint-tanuki.png)

---

![absolute link](/foo/paint-tanuki.png)
